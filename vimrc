syntax enable
set number
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set smarttab
set autoindent
set mouse=a

set laststatus=2
set showtabline=2
set noshowmode
