Various configurations for different programs I commonly use

The Monokai colorscheme referenced in `vimrc` is [sickill/vim-monokai](https://github.com/sickill/vim-monokai).

Symlink destination for files:
* `environment.d/` → `$HOME/.config/environment.d`
* `alacritty.yml` → `$HOME/.config/alacritty/alacritty.yml`
* `vimrc` → `$HOME/.config/vimrc`
* `zsh/` → `${ZDOTDIR:-$HOME/.config/zsh/}`

TODO:  
[x] Script creation of symlinks
